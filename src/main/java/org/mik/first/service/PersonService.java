package org.mik.first.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Person;

public class PersonService implements Service<Person> {

    private static final Logger LOG=LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    private static PersonService instance;


    private PersonService() {

    }

    @Override
    public void pay(Person person) {
        LOG.trace("Enter PersonService.pay/TRACE");
        LOG.debug("Enter PersonService.pay/DEBUG");
        LOG.info("Enter PersonService.pay/INFO");
        LOG.warn("Enter PersonService.pay/WARN");
        LOG.error("Enter PersonService.pay/ERROR");
    }

    @Override
    public void receiveService(Person person) {

    }

    public synchronized static PersonService getInstance() {
        if (instance==null)
            instance=new PersonService();
        return instance;
    }
}
