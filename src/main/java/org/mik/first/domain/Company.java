package org.mik.first.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerialization;

@Data
@NoArgsConstructor
@SuperBuilder
@XMLSerialization(key = "HatedCompany")
public class Company extends  Client {

    @XMLElement(key = "SecretId")
    private String taxId;

}
