package org.mik.first.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerialization;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@XMLSerialization
public class Country {

    @XMLElement
    private String name;
    @XMLElement
    private String sign;
}
