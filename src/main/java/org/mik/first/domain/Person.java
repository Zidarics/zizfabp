package org.mik.first.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerialization;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@XMLSerialization
public class Person extends Client {

    @XMLElement
    private String personalId;

}

