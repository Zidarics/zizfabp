package org.mik.first.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLInit;
import org.mik.first.export.xml.XMLSerialization;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@XMLSerialization
public class Client {

    private static final Logger LOG= LogManager.getLogger();
      private final static Boolean DEBUG_TEMPORARY = true;

    @XMLElement
    private String name;
    @XMLElement
    private String address;
    @XMLElement
    private Country country;

    @XMLInit
    public void xmlInit() {
           LOG.debug("Enter Client.xmlInit ");
    }
}
