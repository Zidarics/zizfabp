package org.mik.first;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Client;
import org.mik.first.domain.Company;
import org.mik.first.domain.Country;
import org.mik.first.domain.Person;
import org.mik.first.export.xml.XMLGenerator;
import org.mik.first.service.PersonService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Control {

    private static final Logger LOG= LogManager.getLogger();
      private final static Boolean DEBUG_TEMPORARY = true;


      private PersonService personService;
      private XMLGenerator xmlGenerator;

    public Control() {
        this.personService = PersonService.getInstance();
        this.xmlGenerator=new XMLGenerator();
    }

    public void start() {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter Control.start ");
        List<Client> cl=createDummyList().stream()
                .map(this::convertFromString)
                .collect(Collectors.toList());
        cl.stream().forEach(client ->
                System.out.println(
                    this.xmlGenerator.convert2XML(client)
                )
        );
    }

    private Client convertFromString(String[] strings) {
        switch (strings[0]) {
            case "P" : return Person.builder().
                    name(strings[1])
                    .country(
                       Country.builder().name(strings[4]).sign(strings[5]).build()
                    ).address(strings[2])
                    .personalId(strings[3])
                    .build();
            case "C" : return Company.builder()
                    .name(strings[1])
                    .country(Country.builder()
                             .name(strings[4])
                              .sign(strings[5])
                              .build())
                    .address(strings[2])
                    .taxId(strings[3])
                    .build();
            default:
                if (DEBUG_TEMPORARY)
                   LOG.debug("Control.convertFromString error, unknown flag: "+strings[0]);
                throw new RuntimeException("Control.convertFromString error, unknown flag: "
                                            +strings[0]);
        }
    }

    private List<String[]> createDummyList() {
        List<String[]> dummyList=new ArrayList<>();
        dummyList.add(new String[] {"P", "Linus Torvalds", "USA",
                                    "42234560TL", "Hungary", "HU"});
        dummyList.add(new String[] {"P", "Tricia McMillan", "Earth",
                                    "42234560ML", "Hungary", "HU"});
        dummyList.add(new String[] {"P", "Ford Prefect", "Betelgeuse",
                "42234560TB", "Hungary", "HU"});

        dummyList.add(new String[] {"C", "Google", "USA",
                "42234560GG", "Usa", "US"});
        dummyList.add(new String[] {"C", "Oracle", "USA",
                "42234560OR", "Usa", "US"});
        dummyList.add(new String[] {"C", "Microsoft", "USA",
                "42234560MS", "Usa", "US"});
        dummyList.add(new String[] {"C", "KGB", "Russia",
                "42234560KGB", "Russia", "RU"});
        return dummyList;
    }


}
